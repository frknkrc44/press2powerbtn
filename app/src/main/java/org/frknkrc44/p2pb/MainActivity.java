package org.frknkrc44.p2pb;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.PrintWriter;
import java.util.Timer;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        java.lang.Process runtime = null;
        PrintWriter ps = null;
        try {
            runtime = Runtime.getRuntime().exec("su");
            ps = new PrintWriter(runtime.getOutputStream());
            ps.write("input keyevent 26\n");
            ps.flush();
            ps.close();
        } catch (Throwable e) {
            Toast.makeText(this, "APP ERROR!\nThis app requires root and 'input' command", Toast.LENGTH_LONG).show();
        } finally {
            if(runtime != null) {
                try {
                    runtime.waitFor();
                    runtime.destroy();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            finish();
        }
    }
}